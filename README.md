Datasets used for comparison of subgraph mining tools in multiple and single graph settings.

To compare multiple graphs setting tools we used a protein structure graph dataset derived from all RCSB PDB non-redundant 
structures. 
	- NRPDB_GR.txt (graph file)
	- NRPDB_LA.txt (labels file)
	

To compare single graph setting tools we used the protein--protein interaction graph of Escherichia coli as present in the 
Bacteriome database[1] with a confidence cut-off of 0.8.
	-bacteriome_combined_interactions_may2016_uniprot.txt


[1] Su, C., Peregrin-Alvarez, J. M., Butland, G., Phanse, S., Fong, V., Emili, A., & Parkinson, J. (2008). 
Bacteriome.org—an integrated protein interaction database for E. coli. Nucleic Acids Research, 36(Database issue), 
D632–D636. http://doi.org/10.1093/nar/gkm807
